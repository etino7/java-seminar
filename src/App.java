import java.util.List;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        String tekst;

        Repozitorij repo = Repozitorij.novaInstanca();

        Autor tolkien = new Autor("J. R. R. ", "Tolkien", 1982);
        Autor martin = new Autor("George R. R. ", "Martin", 1948);
        Autor gaiman = new Autor("Neil", "Gaiman", 1960);

        Knjiga gods = new Knjiga("American Gods", 2001);
        Knjiga lotr = new Knjiga("Gospodar Prstenova", 1954);
        Knjiga igra = new Knjiga("Igra prijestlja", 1996);

        repo.umetniAutora(tolkien);
        repo.umetniAutora(martin);
        repo.umetniAutora(gaiman);

        repo.umetniKnjigu(gods);
        repo.umetniKnjigu(lotr);
        repo.umetniKnjigu(igra);

        Scanner scanner = new Scanner(System.in);

        System.out.println("Unesite pojam za pretrazivanje");
        tekst = scanner.nextLine();

        System.out.println("Rezultati pretrazivanja");
        repo.pretrazi(tekst).forEach(stavka -> {
            stavka.prikazNaziva();
        });


    }
}
