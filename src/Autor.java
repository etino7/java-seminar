public class Autor implements StavkaPretrazivanja{

    private String id;
    private String ime;
    private String prezime;
    private int godinaRodjenja;
    private String biografija;

    public Autor(String ime, String prezime, int godinaRodjenja){
        this.ime = ime;
        this.prezime = prezime;
        this.godinaRodjenja = godinaRodjenja;
    }

    @Override
    public void prikazNaziva() {
        System.out.print(ime + " " + prezime);
    }

    @Override
    public void prikazGodine() {
        System.out.print(godinaRodjenja);
    }

    @Override
    public void prikazSazetka() {
        System.out.print(biografija);
    }

    @Override
    public String vratiNaziv() {
        return ime + prezime;
    }

    public String getBiografija() {
        return biografija;
    }

    public String getId() {
        return id;
    }

    public int getGodinaRodjenja() {
        return godinaRodjenja;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }
}
