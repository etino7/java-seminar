
public class Knjiga implements StavkaPretrazivanja{

    private String ISBN;
    private String naziv;
    private String autor;
    private int godinaIzlaska;
    private String sazetak;

    public Knjiga(String naziv,int godinaIzlaska){
        this.naziv = naziv;
        this.godinaIzlaska = godinaIzlaska;
    }


    @Override
    public void prikazNaziva() {
        System.out.print(naziv);
    }

    @Override
    public void prikazGodine() {
        System.out.print(godinaIzlaska);
    }

    @Override
    public void prikazSazetka() {
        System.out.print(sazetak);
    }

    @Override
    public String vratiNaziv() {
        return naziv;
    }

    public String getISBN() {
        return ISBN;
    }

    public String getAutor() {
        return autor;
    }

    public String getNaziv() {
        return naziv;
    }

    public int getGodinaIzlaska() {
        return godinaIzlaska;
    }

    public String getSazetak() {
        return sazetak;
    }
}
