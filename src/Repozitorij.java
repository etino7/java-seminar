import java.util.ArrayList;
import java.util.List;

public class Repozitorij {

    private static Repozitorij repo;
    private List<StavkaPretrazivanja> stavke = new ArrayList<>();

    private Repozitorij(){}

    public static Repozitorij novaInstanca(){
        if(repo == null)
            repo = new Repozitorij();

        return repo;
    }

    public List<StavkaPretrazivanja> pretrazi(String uneseniTekst){
        List<StavkaPretrazivanja> rezultati = new ArrayList<>();
         stavke.forEach(stavkaPretrazivanja ->  {
             if(stavkaPretrazivanja.vratiNaziv().toLowerCase().contains(uneseniTekst.toLowerCase()))
                 rezultati.add(stavkaPretrazivanja);
        });

         return rezultati;

    }

    public void umetniKnjigu(Knjiga knjiga){
        stavke.add(knjiga);
    }

    public void umetniAutora(Autor autor){
        stavke.add(autor);
    }
}
