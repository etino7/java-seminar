public interface StavkaPretrazivanja {

    void prikazNaziva();

    void prikazGodine();

    void prikazSazetka();

    String vratiNaziv();

}
